/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.pawarit.me.tictactoew3;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class TicTacToeW3 {

    static Scanner sc = new Scanner(System.in);
    private static char table[][] = new char[3][3];
    private static char currentPlayer;
    private static int turn = 1;
    private static boolean gameLoop = true;
    private static int row;
    private static int col;

    public static void main(String[] args) {
        showWelcome();
        createTable();
        //loop until gameLoop = false;
        while (gameLoop) {
            showTable();
            showTurn();
            inputRowCol();
            if (checkWin()) {
                showTable();
                System.out.println(currentPlayer + " Win !!!!");
                gameLoop = false;
            } else if (checkDraw()) {
                showTable();
                System.out.println("Draw !!!");
            }
            turn++;
        }
    }

    //print welcome to xo game
    private static void showWelcome() {
        System.out.println("Welcome to XO game");
    }

    //create table assign '-' to table
    public static void createTable() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                table[r][c] = '-';
            }
        }
    }

    //print out table
    private static void showTable() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println();
        }
    }

    //show turn example: Turn X
    private static void showTurn() {
        if (turn % 2 != 0) {
            currentPlayer = 'X';
            System.out.println("Turn " + currentPlayer);
        } else {
            currentPlayer = 'O';
            System.out.println("Turn " + currentPlayer);
        }
    }

    //input row and col and assign 'X' or 'O' to table
    private static void inputRowCol() {
        try {
            System.out.print("Please input row, col : ");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = currentPlayer;
            } else {
                System.out.println("This position is already taken. Try again.");
                turn--;
            }
        } catch (Exception e) {
            showTable();
            showTurn();
            System.out.println("Please input number between(1-3)");
            inputRowCol();
        }
    }

    //check if table is a draw
    public static boolean checkDraw() {
        if (turn == 9) {
            gameLoop = false;
            return true;
        }
        return false;
    }

    //check win return true if win, return false if not win
    public static boolean checkWin() {
        return checkVertical() || checkHorizontal() || checkDiagonal();
    }

    //check vertical win
    public static boolean checkVertical() {
        return table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer;
    }

    //check horizontal win
    public static boolean checkHorizontal() {
        return table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer;
    }

    //check diagonal win
    public static boolean checkDiagonal() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    // Getter and setter methods for testing purposes
    public char[][] getTable() {
        return table;
    }

    public void setTable(char[][] table) {
        this.table = table;
    }

    public char getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(char currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setCol(int col) {
        this.col = col;
    }
}
