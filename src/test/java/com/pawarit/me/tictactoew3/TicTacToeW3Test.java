/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.pawarit.me.tictactoew3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author admin
 */
public class TicTacToeW3Test {
    private TicTacToeW3 game;
    public TicTacToeW3Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    void setUp() {
        game = new TicTacToeW3();
        game.createTable();
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of main method, of class TicTacToeW3.
     */
    @Test
    void testCreateTable() {
        char[][] expectedTable = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
        };
        assertArrayEquals(expectedTable, game.getTable());
    }
    @Test
    void testCheckDraw() {
        game.setTurn(9);
        assertTrue(game.checkDraw());
    }
    @Test
    void testCheckVerticalWin() {
        for (int col = 0; col < 3; col++) {
            game.createTable();
            game.getTable()[0][col] = 'X';
            game.getTable()[1][col] = 'X';
            game.getTable()[2][col] = 'X';
            game.setCurrentPlayer('X');
            game.setCol(col);
            assertTrue(game.checkVertical(), "Failed for column " + col);
        }
    }
    @Test
    void testCheckHorizontalWin() {
        for (int row = 0; row < 3; row++) {
            game.createTable();
            game.getTable()[row][0] = 'X';
            game.getTable()[row][1] = 'X';
            game.getTable()[row][2] = 'X';
            game.setCurrentPlayer('X');
            game.setRow(row);
            assertTrue(game.checkHorizontal(), "Failed for row " + row);
        }
    }
    
    @Test
    void testCheckDiagonalWin() {
        game.getTable()[0][0] = 'X';
        game.getTable()[1][1] = 'X';
        game.getTable()[2][2] = 'X';
        game.setCurrentPlayer('X');
        assertTrue(game.checkDiagonal(), "Failed for main diagonal");

        game.createTable(); // Reset the table

        game.getTable()[0][2] = 'X';
        game.getTable()[1][1] = 'X';
        game.getTable()[2][0] = 'X';
        assertTrue(game.checkDiagonal(), "Failed for anti-diagonal");
    }
    @Test
    void testNoWin() {
        game.getTable()[0][0] = 'X';
        game.getTable()[0][1] = 'O';
        game.getTable()[0][2] = 'X';
        game.getTable()[1][0] = 'O';
        game.getTable()[1][1] = 'X';
        game.getTable()[1][2] = 'O';
        game.getTable()[2][0] = 'O';
        game.getTable()[2][1] = 'X';
        game.getTable()[2][2] = 'O';
        game.setCurrentPlayer('X');
        assertFalse(game.checkWin());
    }
}
